#include <msp430.h>

unsigned int voltage[4];

int main(void)
{
    WDTCTL = WDTPW + WDTHOLD;         // Stop WDT
    P6SEL  = BIT0;                    // P6.1 ADC option select
                                      //  P1DIR |= BIT0;                            // P1.0 output
    ADC12CTL0 = ADC12SHT02 + ADC12ON; // Sampling time, ADC12 on
    ADC12CTL1 = ADC12SHP;             // Use sampling timer
                                      //  ADC12IE = ADC12IE1;                           // Enable interrupt p6.1
    ADC12MCTL0 = ADC12INCH_0;         // A1 as adc input channel p6.0
    //  ADC12MCTL1 = ADC12INCH_1; //A1 as adc input channel p6.1
    ADC12CTL0 |= ADC12ENC;

    int i;
    volatile unsigned long tmp;
    while (1) {
        tmp = 0;
        for (i = 0; i < 4; i++) {
            ADC12CTL0 |= ADC12SC; // Start sampling/conversion
            voltage[i] = ADC12MEM0;
            tmp += voltage[i];
        }
        tmp = tmp >> 2;
        //    while(!(ADC12IFG & BIT1 ));
        //    __bis_SR_register(LPM0_bits + GIE);     // LPM0, ADC12_ISR will force exit
        __no_operation(); // For debugger
    }
}